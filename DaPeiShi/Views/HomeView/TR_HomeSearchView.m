//
//  TR_HomeSearchView.m
//  DaPeiShi
//
//  Created by admin on 2019/8/29.
//  Copyright © 2019 candy.chen. All rights reserved.
//

#import "TR_HomeSearchView.h"

@interface TR_HomeSearchView ()
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end

@implementation TR_HomeSearchView

- (void)awakeFromNib{
    [super awakeFromNib];
    self.label1.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBlack];
    self.label2.font = [UIFont systemFontOfSize:18 weight:UIFontWeightBlack];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
