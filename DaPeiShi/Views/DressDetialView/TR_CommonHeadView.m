//
//  TR_CommonHeadView.m
//  DaPeiShi
//
//  Created by admin on 2019/8/29.
//  Copyright © 2019 candy.chen. All rights reserved.
//

#import "TR_CommonHeadView.h"

@interface TR_CommonHeadView ()

@end

@implementation TR_CommonHeadView
- (IBAction)gobackAction:(id)sender {
    if (self.headAction) {
        self.headAction(BackAction);
    }
}
- (IBAction)searchAction:(id)sender {
    if (self.headAction) {
        self.headAction(SearchAction);
    }
}
- (IBAction)seeMoreAction:(id)sender {
    if (self.headAction) {
        self.headAction(SeeMoreAction);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
