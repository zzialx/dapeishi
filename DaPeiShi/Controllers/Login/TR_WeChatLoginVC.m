//
//  TR_WeChatLoginVC.m
//  DaPeiShi
//
//  Created by admin on 2019/8/29.
//  Copyright © 2019 candy.chen. All rights reserved.
//

#import "TR_WeChatLoginVC.h"

@interface TR_WeChatLoginVC ()

@end

@implementation TR_WeChatLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)chatLoginAction:(id)sender {
    if ([WXApi isWXAppInstalled]) {
        SendAuthReq *req = [[SendAuthReq alloc] init];
        req.scope = @"snsapi_userinfo";
        req.state = @"App";
        [WXApi sendReq:req];
    }else {
        NSLog(@"请先安装微信客户端");
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
