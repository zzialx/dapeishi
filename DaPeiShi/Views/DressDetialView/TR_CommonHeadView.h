//
//  TR_CommonHeadView.h
//  DaPeiShi
//
//  Created by admin on 2019/8/29.
//  Copyright © 2019 candy.chen. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ClickActionType) {
    BackAction     = 0,
    SearchAction,
    SeeMoreAction
};

NS_ASSUME_NONNULL_BEGIN

typedef void(^clickHeadCommon)(ClickActionType actionType);

@interface TR_CommonHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleab;

@property(copy,nonatomic)clickHeadCommon headAction;

@end

NS_ASSUME_NONNULL_END
