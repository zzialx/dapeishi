//
//  TR_SortHeadView.h
//  DaPeiShi
//
//  Created by xq的电脑 on 2019/6/22.
//  Copyright © 2019 candy.chen. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, ContionType) {
    ContionType_Default = 0,
    ContionType_Fans,
    ContionType_Style,
    ContionType_Leavel,
};

typedef void(^selectContion)(ContionType contionType);
@interface TR_SortHeadView : UIView

@property(copy,nonatomic)selectContion selectContion;

@end

NS_ASSUME_NONNULL_END
